
### Usage

```
npm install
npm start
```

Now edit `src/App.js`.  
Your changes will appear without reloading the browser

### Linting

This boilerplate project includes React-friendly ESLint configuration.

```
npm run lint
```