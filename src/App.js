import React, {Component} from 'react';
import MessageList from './components/MessageList';

import styles from './scss/style.scss';

class App extends Component {
  constructor() {
    super();

    let mock_data = [
      {
        'Twitter': {
          'user': {
            'profile_image_url': '//gravatar.com/avatar/44842684fe6e0b841b160722e36d534a',
            'screen_name': '@acolpitts',
            'name': 'Adam Colpitts'

          },
          'content': 'React/ES6 implementation of a basic message list component. #react #sass #webpack #hmr #hotreload'
        }
      },
      {
        'Facebook': {
          'from': {
            'name': 'John Smith',
            'handle': 'john.smith',
            'image_url': '//www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?f=y'
          },
          'message': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec lacinia nulla mauris, vel auctor tellus pulvinar non.'
        }
      }
    ];

    this.state = {
      data: mock_data
    };

    // mock data stream
    let self = this;
    let i = 0;
    let timer = setInterval(function () {
      mock_data.push(mock_data[Math.round(Math.random())]);
      self.setState({data: mock_data});
      if (++i === 5) {
        window.clearInterval(timer);
      }
    }, 2000)

  }

  render() {
    return (
      <div className="l-constrained">
          <MessageList messages={this.state.data}/>
      </div>
    );
  }
}

export default App;